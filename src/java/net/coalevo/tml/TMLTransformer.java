/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml;

import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Provides a helper class to transform TML to available targets.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class TMLTransformer {

  public String toANSI(String str) throws TMLException {
    final StringWriter w = new StringWriter();
    final StringReader r = new StringReader(str);

    DefaultTMLEngine engine = new DefaultTMLEngine(r);
    try {
      engine.getContext().pushInputName("<<fragment>>");
      engine.setFilename("<<fragment>>");

      engine.translate(new ANSITarget(w));
      w.flush();
      return w.toString();

    } catch (Exception e) {
      throw new TMLException(e,engine.getContext().getLine());
    }

  }//toANSI

  public void toANSI(Reader r, Writer w) throws TMLException {

    DefaultTMLEngine engine = new DefaultTMLEngine(r);
    try {
      engine.getContext().pushInputName("<<fragment>>");
      engine.setFilename("<<fragment>>");
      engine.translate(new ANSITarget(w));
      w.flush();
    } catch (Exception e) {
      throw new TMLException(e,engine.getContext().getLine());
    }
  }//toANSI

  public String toHTML(String str) throws TMLException {
    final StringWriter w = new StringWriter();
    final StringReader r = new StringReader(str);

    DefaultTMLEngine engine = new DefaultTMLEngine(r);
    try {
      engine.getContext().pushInputName("<<fragment>>");
      engine.setFilename("<<fragment>>");

      engine.translate(new HTMLTarget(w));
      w.flush();
      return w.toString();

    } catch (Exception e) {
      throw new TMLException(e,engine.getContext().getLine());
    }

  }//toHTML

  public void toHTML(Reader r, Writer w) throws TMLException {
    DefaultTMLEngine engine = new DefaultTMLEngine(r);

    try {
      engine.getContext().pushInputName("<<fragment>>");
      engine.setFilename("<<fragment>>");
      engine.translate(new HTMLTarget(w));
      w.flush();
    } catch (Exception e) {
      throw new TMLException(e,engine.getContext().getLine());
    }
  }//toHTML

  public String toBBCode(String str) throws TMLException {
    final StringWriter w = new StringWriter();
    final StringReader r = new StringReader(str);

    DefaultTMLEngine engine = new DefaultTMLEngine(r);
    try {
      engine.getContext().pushInputName("<<fragment>>");
      engine.setFilename("<<fragment>>");

      engine.translate(new BBCodeTarget(w));
      w.flush();
      return w.toString();

    } catch (Exception e) {
      throw new TMLException(e,engine.getContext().getLine());
    }

  }//toBBCode

  public void toBBCode(Reader r, Writer w) throws TMLException {

    DefaultTMLEngine engine = new DefaultTMLEngine(r);
    try {
      engine.getContext().pushInputName("<<fragment>>");
      engine.setFilename("<<fragment>>");
      engine.translate(new BBCodeTarget(w));
      w.flush();
    } catch (Exception e) {
      throw new TMLException(e,engine.getContext().getLine());
    }
  }//toBBCode

  public String toTelnetd(String str) throws TMLException {
    final StringWriter w = new StringWriter();
    final StringReader r = new StringReader(str);

    DefaultTMLEngine engine = new DefaultTMLEngine(r);
    try {
      engine.getContext().pushInputName("<<fragment>>");
      engine.setFilename("<<fragment>>");

      engine.translate(new TelnetDTarget(w));
      w.flush();
      return w.toString();

    } catch (Exception e) {
      throw new TMLException(e,engine.getContext().getLine());
    }

  }//toTelnetd

  public void toTelnetd(Reader r, Writer w) throws TMLException {
    DefaultTMLEngine engine = new DefaultTMLEngine(r);
    try {
      engine.getContext().pushInputName("<<fragment>>");
      engine.setFilename("<<fragment>>");
      engine.translate(new TelnetDTarget(w));
      w.flush();
    } catch (Exception e) {
      throw new TMLException(e,engine.getContext().getLine());
    }
  }//toTelnetd

    public String toText(String str) throws TMLException {
    final StringWriter w = new StringWriter();
    final StringReader r = new StringReader(str);

    DefaultTMLEngine engine = new DefaultTMLEngine(r);
    try {
      engine.getContext().pushInputName("<<fragment>>");
      engine.setFilename("<<fragment>>");

      engine.translate(new TextTarget(w));
      w.flush();
      return w.toString();

    } catch (Exception e) {
      throw new TMLException(e,engine.getContext().getLine());
    }

  }//toTelnetd

  public void toText(Reader r, Writer w) throws TMLException {
    DefaultTMLEngine engine = new DefaultTMLEngine(r);
    try {
      engine.getContext().pushInputName("<<fragment>>");
      engine.setFilename("<<fragment>>");
      engine.translate(new TextTarget(w));
      w.flush();
    } catch (Exception e) {
      throw new TMLException(e,engine.getContext().getLine());
    }
  }//toTelnetd

}//class TMLTransformer
