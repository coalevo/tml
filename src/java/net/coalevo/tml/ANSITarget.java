/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml;

import net.coalevo.text.util.ANSIHelper;
import net.coalevo.text.util.MarkupLanguageFilter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;

class ANSITarget
    extends NOPTarget
    implements TMLTranslator {

  private ANSIHelper m_Ansi = new ANSIHelper();
  private Stack<AList> m_List = new Stack<AList>();
  private Table m_Table;
  private StringBuilder m_Col;

  public ANSITarget() {
    this(new BufferedWriter(new OutputStreamWriter(System.out)));
  }//ANSITarget

  public ANSITarget(Writer out) {
    this.m_Out = out;
  }//ANSITarget

  public String getTargetLanguage() {
    return "ANSI";
  }//getTargetLanguage

  /**
   * Handle header?
   */
  public void begin() {
  }//begin

  /**
   * Handle tail?
   */
  public void end() {
    try {
      m_Out.flush();
    }
    catch (IOException ioe) {
      System.err.println("Problem writing ANSI output");
      ioe.printStackTrace(System.err);
    }
  }//end

  protected String extractLastCommand(String text) {
    if (text == null) {
      return null;
    }
    int i = text.lastIndexOf("<");
    if (i < 0) {
      return null;
    }
    int begin = i;
    i++;
    int end = 0;
    while (i < text.length() && text.charAt(i) != '>') {
      i++;
    }
    if (i >= text.length()) {
      return null; // <... not terminated
    }
    end = i;
    return text.substring(begin, end);
  }//extractLastCommand

  public void write(String text) {
    if (m_Col != null) {
      m_Col.append(text);
      return;
    } else if (!m_List.isEmpty()) {
      super.write(MarkupLanguageFilter.filterWS(text));
    } else {
      super.write(text);
    }
  }//write

  public void text(String t) {
    write(t);
  }//text

  public void bold(String t) {
    write(m_Ansi.startStyle(ANSIHelper.BOLD));
    write(t);
    write(m_Ansi.endStyle());
  }//bold

  public void italic(String t) {
    write(m_Ansi.startStyle(ANSIHelper.ITALIC));
    write(t);
    write(m_Ansi.endStyle());
  }//italic

  public void underlined(String t) {
    write(m_Ansi.startStyle(ANSIHelper.UNDERLINED));
    write(t);
    write(m_Ansi.endStyle());
  }//underlined

  public void tt(String t) {
    write(t);
  }//tt

  public void beginListItem(int level) {
    if (!m_List.isEmpty()) {
      //indent by level
      super.write(getString(' ', 4 * level));
      write(((AList) m_List.peek()).next());
    }
  }//beginListItem

  public void endListItem(int level) {
    linebreak();
  }//endListItem

  public void begin_ul(int level) {
    linebreak();
    m_List.push(new BulletList());
  }//begin_ul

  public void end_ul(int level) {
    if (!m_List.isEmpty()) {
      m_List.pop();
    }
    linebreak();
  }//end_ul

  public void begin_ol(int level) {
    linebreak();
    m_List.push(new NumericList());
  }//begin_ol

  public void end_ol(int level) {
    if (!m_List.isEmpty()) {
      m_List.pop();
    }
    linebreak();
  }//end_ol

  public void paragraph() {
    linebreak();
  }//paragraph

  public void linebreak() {
    super.write(LINEBREAK);
  }//linebreak

  public void blankline() {
    write(LINEBREAK);
    write(LINEBREAK);
  }//blankline

  public void code(String c) {
    super.write("\n\n-- Code --\n");
    super.write(c);
    super.write("\n-- End Code --\n\n");
  }//code

  public void verbatim(String rawOutput) {
    write(rawOutput);
  }//verbatim

  public void blockquote(String q) {
    //indent?
    super.write("\n\n-- Quote --\n");
    write(m_Ansi.startStyle(ANSIHelper.BOLD));
    write(q);
    write(m_Ansi.endStyle());
    super.write("\n-- End Quote --\n\n");
  }//blockquote

  public void link(String url, String title) {
    title = filterContent(title);
    write(" ");
    write((title == null) ? "" : title);
    write(m_Ansi.startFgColor(ANSIHelper.GREEN));
    write(" ==> ");
    write(m_Ansi.endFgColor());
    write(m_Ansi.startFgColor(ANSIHelper.CYAN));
    write(url);
    write(m_Ansi.endFgColor());
    write(" ");
  }//link

  public void title(String title) {
    beginSection(title, 0);
  }//title

  public void beginSection(String title, int level) {
    title = filterContent(title);
    write(m_Ansi.startStyle(ANSIHelper.BOLD));
    linebreak();
    write(m_Ansi.startFgColor(ANSIHelper.WHITE));
    write(title);
    write(m_Ansi.endFgColor());
    linebreak();
    switch (++level) {
      case 1:
        write(m_Ansi.startFgColor(ANSIHelper.RED));
        write(getString('=', title.length()));
        write(m_Ansi.endFgColor());
        break;
      case 2:
        write(m_Ansi.startFgColor(ANSIHelper.GREEN));
        write(getString('=', title.length()));
        write(m_Ansi.endFgColor());
        break;
      case 3:
        write(m_Ansi.startFgColor(ANSIHelper.YELLOW));
        write(getString('-', title.length()));
        write(m_Ansi.endFgColor());
        break;

      default:
        write(m_Ansi.startFgColor(ANSIHelper.WHITE));
        write(getString('-', title.length()));
        write(m_Ansi.endFgColor());
    }
    write(m_Ansi.endStyle());
    linebreak();
  }//beginSection

  public void begin_table() {
    m_Table = new ANSITable();
    m_Col = new StringBuilder();
  }//begin_table

  public void end_table() {
    m_Table.addCol(m_Col.toString());
    m_Col = new StringBuilder();
    m_Table.nextRow();
    m_Col = null;
    //printout
    super.write(m_Table.toString());
    m_Table = null;
  }//end_table

  public void col() {
    m_Table.addCol(m_Col.toString());
    m_Col = new StringBuilder();
  }//col

  public void row() {
    m_Table.addCol(m_Col.toString());
    m_Col = new StringBuilder();
    m_Table.nextRow();
  }//row

  protected String filterContent(String q) {
    return MarkupLanguageFilter.filterMLWS(q);
  }//filterContent

  private static final String getString(char c, int times) {
    if (times == 0) {
      return "";
    }
    final StringBuilder sbuf = new StringBuilder();
    while (times-- > 0) {
      sbuf.append(c);
    }
    return sbuf.toString();
  }//getString

  public static final String LINEBREAK = "\n";

  private static abstract class AList {

    public abstract String next();

  }//AList

  private static class NumericList extends AList {

    int m_Num = 1;

    public String next() {
      return m_Num++ + ". ";
    }//next
  }//NumericList

  private static class AlphanumericList extends AList {

    char m_Alpha = 'a';

    public String next() {
      return m_Alpha++ + ". ";
    }//next

  }//AlphanumericList

  private static class BulletList extends AList {

    public String next() {
      return "* ";
    }
  }//BulletList

  private static class ANSITable
      implements Table {

    protected int m_RIdx;
    protected List<List<String>> m_Rows;
    protected Map<Integer, Integer> m_ColSizes;

    public ANSITable() {
      m_ColSizes = new HashMap<Integer, Integer>();
      m_Rows = new ArrayList<List<String>>();
      m_RIdx = 0;
      m_Rows.add(new ArrayList<String>());
    }//constructor

    public void addCol(String str) {
      str = MarkupLanguageFilter.filterMLWS(str);
      List<String> cols = m_Rows.get(m_RIdx);

      int colidx = cols.size();
      int collen = ANSIHelper.getVisibleLength(str);

      if (m_ColSizes.containsKey(colidx)) {
        int ll = m_ColSizes.get(colidx);
        if (collen > ll) {
          m_ColSizes.put(colidx, collen);
        }
      } else {
        m_ColSizes.put(colidx, collen);
      }
      cols.add(str);
    }//addCol

    public void nextRow() {
      m_Rows.add(new ArrayList<String>());
      m_RIdx++;
    }//nextRow


    public String toString() {
      StringBuilder sb = new StringBuilder();
      //1. Prepare table top and bottom
      StringBuilder tabletop = new StringBuilder();
      StringBuilder tablebot = new StringBuilder();
      tabletop.append(LEFT_UPPER);
      tablebot.append(LEFT_LOWER);
      for (int i = 0; i < m_ColSizes.size(); i++) {
        int width = m_ColSizes.get(i) + 1;
        tabletop.append(getString(HORIZONTAL, width + 1));
        tablebot.append(getString(HORIZONTAL, width + 1));
        if (i < m_ColSizes.size() - 1) {
          tabletop.append(RIGHT_UPPER);
          tablebot.append(RIGHT_LOWER);
        } else {
          tabletop.append(MID_UPPER);
          tablebot.append(MID_LOWER);
        }
      }

      //2. Add table top
      sb.append(LINEBREAK);
      sb.append(tabletop.toString());
      sb.append(LINEBREAK);

      //3. Add rows
      for (int n = 0; n < m_Rows.size(); n++) {
        List<String> cols = m_Rows.get(n);
        if (cols.isEmpty()) {
          break;
        }
        boolean islastrow = (n == m_Rows.size() - 2);

        StringBuilder row = new StringBuilder();
        StringBuilder rowsep = new StringBuilder();

        int numcols = cols.size();
        for (int i = 0; i < numcols; i++) {
          int width = m_ColSizes.get(i) + 1;
          String colstr = cols.get(i);
          if (i == 0) {
            rowsep.append(LEFT_MID);
            rowsep.append(getString(HORIZONTAL, width + 1));
            //Row first with a vertical bar
            row.append(VERTICAL);
          } else {
            rowsep.append(MID_MID);
            rowsep.append(getString(HORIZONTAL, width + 1));
          }
          if (i == (numcols - 1)) {
            rowsep.append(RIGHT_MID);
          }

          appendPadded(colstr, width, row);
          row.append(VERTICAL);
        }
        //Add row
        sb.append(row.toString());
        sb.append(LINEBREAK);
        //Add Separator
        if (!islastrow) {
          sb.append(rowsep);
          sb.append(LINEBREAK);
        }

      }

      //4. Add table bottom
      sb.append(tablebot.toString());
      sb.append(LINEBREAK);

      return sb.toString();
    }//toString

    public void appendPadded(String str, int width, StringBuilder sbuf) {
      int len = ANSIHelper.getVisibleLength(str);
      sbuf.append(" ");
      sbuf.append(str);
      sbuf.append(getString(' ', width - len));
    }//appendPadded


    private static char HORIZONTAL = '-';
    private static char VERTICAL = '|';

    private static char LEFT_UPPER = '+';
    private static char RIGHT_UPPER = '+';
    private static char MID_UPPER = '+';

    private static char LEFT_MID = '+';
    private static char RIGHT_MID = '+';
    private static char MID_MID = '+';

    private static char LEFT_LOWER = '+';
    private static char RIGHT_LOWER = '+';
    private static char MID_LOWER = '+';

    /* Extended ASCII Set
    private static char HORIZONTAL = (char)196;
    private static char VERTICAL = (char)179;

    private static char LEFT_UPPER = (char)218;
    private static char RIGHT_UPPER = (char)191;
    private static char MID_UPPER = (char)194;

    private static char LEFT_MID = (char)195;
    private static char RIGHT_MID = (char)180;
    private static char MID_MID = (char) 197;

    private static char LEFT_LOWER = (char)192;
    private static char RIGHT_LOWER = (char)217;
    private static char MID_LOWER = (char)193;
    */

  }//Table


}//class ANSITarget
