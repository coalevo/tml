/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml;

/**
 * Provides an interface for collecting a table.
 * <p/>
 * Extend with a <tt>toString()</tt> that formats the
 * output properly.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
interface Table {

  public void addCol(String str);

  public void nextRow() ;

}//class Table