/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml;

/**
 * Exception thrown if TML source is invalid.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class TMLException
    extends Exception {

private int m_Line = -1;

  public TMLException() {
  }//TMLException

  public TMLException(Throwable cause) {
    super(cause);
  }//TMLException

  public TMLException(String message) {
    super(message);
  }//TMLException

  public TMLException(String message, Throwable cause) {
    super(message, cause);
  }//TMLException

  /**
   * Constructs a new <tt>TMLException</tt> with a message.
   *
   * @param msg  the message.
   * @param line the line number of the input that caused the exception.
   */
  public TMLException(String msg, int line) {
    super(msg);
    m_Line = line;
  }//TMLException

  /**
   * Constructs a new <tt>TMLException</tt> with a message.
   *
   * @param cause the cause.
   * @param line the line number of the input that caused the exception.
   */
  public TMLException(Throwable cause, int line) {
    super(cause);
    m_Line = line;
  }//TMLException

  /**
   * Returns the number of the line that caused the exception.
   *
   * @return the current line number as <tt>int</tt>.
   */
  public int getLine() {
    return m_Line;
  }//getLine

}//class TMLException
