/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml.plugin;

import net.coalevo.tml.TMLContext;
import net.coalevo.tml.TMLPlugin;
import net.coalevo.text.util.ANSIHelper;
import net.coalevo.text.util.TelnetdHelper;

import java.util.Vector;

/**
 * Provides a command to apply style to a given string.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class style
    implements TMLPlugin {

  private ANSIHelper m_Ansi = new ANSIHelper();
  private TelnetdHelper m_Telnetd = new TelnetdHelper();

  public String translate(TMLContext context, Vector args) {
    if (args.size() < 2) {
      System.err.println("Missing string to be stylized or style; line " + context.getLine());
      return null;
    }

    String colstring = (String) args.elementAt(0);
    String stylestring = (String) args.elementAt(1);

    String lang = context.getTranslator().getTargetLanguage();
    if ("HTML".equals(lang)) {
      return htmlStylize(stylestring, colstring);
    }
    if ("ANSI".equals(lang)) {
      return ansiStylize(stylestring, colstring);
    }
    if ("TELNETD".equals(lang)) {
      return telnetdStylize(stylestring, colstring);
    }
    if ("BBCODE".equals(lang)) {
      return bbcodeStylize(stylestring, colstring);
    }
    return colstring;
  }//translate

  public String htmlStylize(String style, String str) {
    return String.format(HTML_STYLE, style, str);
  }//colorize

  public String bbcodeStylize(String style, String str) {
    return String.format(BBCODE_STYLE, style, str);
  }//colorize

  public String ansiStylize(String style, String str) {
    StringBuilder sb = new StringBuilder();
    sb.append(m_Ansi.startStyle(style.charAt(0)));
    sb.append(str);
    sb.append(m_Ansi.endStyle());
    return sb.toString();
  }//ansiStylize

  public String telnetdStylize(String style, String str) {
    StringBuilder sb = new StringBuilder();
    sb.append(m_Telnetd.startStyle(style.charAt(0)));
    sb.append(str);
    sb.append(m_Telnetd.endStyle());
    return sb.toString();
  }//telnetdStylize

  protected static final String HTML_STYLE = "%%output <<<span class=\"%s\">%s</span> >>";
  protected static final String BBCODE_STYLE = "%%output <<[style=%s]%s[/style] >>";

}//class style
