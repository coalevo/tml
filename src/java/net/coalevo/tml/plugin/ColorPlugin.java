/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml.plugin;

import net.coalevo.tml.TMLContext;
import net.coalevo.tml.TMLPlugin;
import net.coalevo.text.util.TelnetdHelper;
import net.coalevo.text.util.ANSIHelper;

import java.util.Vector;
import java.util.Iterator;

/**
 * Provides a base class for color plugins.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class ColorPlugin
    implements TMLPlugin {

  private ANSIHelper m_Ansi = new ANSIHelper();
  private TelnetdHelper m_Telnetd = new TelnetdHelper();


  public String translate(TMLContext context, Vector args) {
    if (args == null || args.size() == 0) {
      System.err.println("Missing string to be colorized; line " + context.getLine());
      return null;
    }
    String colstring = "";
    for (Iterator iterator = args.iterator(); iterator.hasNext();) {
      colstring += (String) iterator.next();
      if(iterator.hasNext()) colstring+=", ";
    }
    String lang = context.getTranslator().getTargetLanguage();
    if ("HTML".equals(lang)) {
      return htmlColorize(getColor(), colstring);
    }
    if ("ANSI".equals(lang)) {
      return ansiColorize(getColor(), colstring);
    }
    if ("TELNETD".equals(lang)) {
      return telnetdColorize(getColor(), colstring);
    }
    if ("BBCODE".equals(lang)) {
      return bbcodeColorize(getColor(), colstring);
    }
    return colstring;
  }//translate


  public String bbcodeColorize(String color, String str) {
    return String.format(BBCODE_COLORIZE, color, str);
  }//bbcodeColorize

  public String htmlColorize(String color, String str) {
    return String.format(HTML_COLORIZE, color, str);
  }//colorize

  public String ansiColorize(String color, String str) {
    StringBuilder sb = new StringBuilder();
    sb.append(m_Ansi.startFgColor(ANSIHelper.getColor(color)));
    sb.append(str);
    sb.append(m_Ansi.endFgColor());
    return sb.toString();
  }//ansiColorize

  public String telnetdColorize(String color, String str) {
    StringBuilder sb = new StringBuilder();
    sb.append(m_Telnetd.startFgColor(m_Telnetd.getColor(color)));
    sb.append(str);
    sb.append(m_Telnetd.endFgColor());
    return sb.toString();
  }//telnetdColorize

  protected abstract String getColor();

  protected static final String HTML_COLORIZE = "%%output <<<font color=\"%s\">%s</font> >>";
  protected static final String BBCODE_COLORIZE = "%%output <<[color=%s]%s[/color] >>";

}//class ColorPlugin
