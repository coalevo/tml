/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml.plugin;

import net.coalevo.tml.TMLContext;
import net.coalevo.tml.TMLPlugin;

import java.util.Vector;

/**
 * Provides a command to apply style to a given string.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class size
    implements TMLPlugin {

  public String translate(TMLContext context, Vector args) {
    if (args.size() < 2) {
      System.err.println("Missing string to be sized or size; line " + context.getLine());
      return null;
    }

    String colstring = (String) args.elementAt(0);
    String sizestring = (String) args.elementAt(1);

    String lang = context.getTranslator().getTargetLanguage();
    if ("HTML".equals(lang)) {
      return htmlSize(sizestring, colstring);
    }
    if ("BBCODE".equals(lang)) {
      return bbcodeSize(sizestring, colstring);
    }
    return colstring;
  }//translate

  public String htmlSize(String size, String str) {
    return String.format(HTML_SIZE, size, str);
  }//colorize

  public String bbcodeSize(String size, String str) {
    return String.format(BBCODE_SIZE, size, str);
  }//colorize

  protected static final String HTML_SIZE = "%%output <<<font size=\"%s\">%s</font> >>";
  protected static final String BBCODE_SIZE = "%%output <<[size=%s]%s[/size] >>";

}//class size
