/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml.plugin;

import net.coalevo.text.util.ANSIHelper;
import net.coalevo.text.util.TelnetdHelper;
import net.coalevo.tml.TMLContext;
import net.coalevo.tml.TMLPlugin;

import java.util.Vector;

/**
 * Provides a command to add a linked img.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class img
    implements TMLPlugin {

  private ANSIHelper m_Ansi = new ANSIHelper();
  private TelnetdHelper m_Telnetd = new TelnetdHelper();

  public String translate(TMLContext context, Vector args) {
    if (args.size() < 1) {
      System.err.println("Missing img source; line " + context.getLine());
      return null;
    }

    String srcstring = (String) args.elementAt(0);
    String altstring = (args.size() == 2) ? (String) args.elementAt(1) : "";

    String lang = context.getTranslator().getTargetLanguage();
    if ("HTML".equals(lang)) {
      return htmlImage(srcstring, altstring);
    }
    if ("ANSI".equals(lang)) {
      return ansiImage(srcstring, altstring);
    }
    if ("TELNETD".equals(lang)) {
      return telnetdImage(srcstring, altstring);
    }
    if ("BBCODE".equals(lang)) {
      return bbcodeImage(srcstring, altstring);
    }
    return altstring;
  }//translate

  public String htmlImage(String src, String alt) {
    return String.format(HTML_IMAGE, src, alt);
  }//htmlImage

  public String bbcodeImage(String src, String alt) {
    if (alt == null || alt.length() == 0) {
      alt = src;
    }
    return String.format(BBCODE_IMAGE, src, alt);
  }//bbcodeImage

  public String ansiImage(String src, String alt) {
    StringBuilder sb = new StringBuilder();
    sb.append("{[Image:");
    if (alt != null && alt.length() > 0) {
      sb.append(alt);
      sb.append(",");
    }
    sb.append("\"");
    sb.append(m_Ansi.startFgColor(ANSIHelper.CYAN));
    sb.append(src);
    sb.append(m_Ansi.endFgColor());
    sb.append("\"]}");
    return sb.toString();
  }//ansiImage

  public String telnetdImage(String src, String alt) {
    StringBuilder sb = new StringBuilder();
    sb.append("{[Image:");
    if (alt != null && alt.length() > 0) {
      sb.append(alt);
      sb.append(",");
    }
    sb.append("\"");
    sb.append(m_Telnetd.startFgColor(TelnetdHelper.CYAN));
    sb.append(src);
    sb.append(m_Telnetd.endFgColor());
    sb.append("\"]}");
    return sb.toString();
  }//telnetdImage

  protected static final String HTML_IMAGE = "%%output <<<img src=\"%s\" alt=\"%s\" /> >>";
  protected static final String BBCODE_IMAGE = "%%output <<[img=\"%s\"]%s[/img] >>";

}//class img
