/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml;

import net.coalevo.text.util.MarkupLanguageFilter;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.io.IOException;

class TextTarget
    extends NOPTarget
    implements TMLTranslator {

  public TextTarget() {
    this(new BufferedWriter(new OutputStreamWriter(System.out)));
  }//ANSITarget

  public TextTarget(Writer out) {
    this.m_Out = out;
  }//ANSITarget

  public String getTargetLanguage() {
    return "TEXT";
  }//getTargetLanguage

  /**
   * Handle header?
   */
  public void begin() {
  }//begin

  /**
   * Handle tail?
   */
  public void end() {
    try {
      m_Out.flush();
    }
    catch (IOException ioe) {
      System.err.println("Problem writing text output");
      ioe.printStackTrace(System.err);
    }
  }//end

  public void text(String t) {
    write(t);
  }//text

  public void bold(String t) {
    write(t);
  }//bold

  public void italic(String t) {
    write(t);
  }//italic

  public void underlined(String t) {
    write(t);
  }//underlined

  public void tt(String t) {
    write(t);
  }//tt

  public void beginListItem(int level) {
  }//beginListItem

  public void endListItem(int level) {
    linebreak();
  }//endListItem

  public void begin_ul(int level) {
  }//begin_ul

  public void end_ul(int level) {
    linebreak();
  }//end_ul

  public void begin_ol(int level) {
  }//begin_ol

  public void end_ol(int level) {
    linebreak();
  }//end_ol

  public void paragraph() {
    linebreak();
  }//paragraph

  public void linebreak() {
    write(LINEBREAK);
  }//linebreak

  public void blankline() {
    linebreak();
    linebreak();
  }//blankline

  public void code(String c) {
  }//code

  public void verbatim(String rawOutput) {
    write(rawOutput);
  }//verbatim

  public void blockquote(String q) {
    //indent?
    write(q);
  }//blockquote

  public void link(String url, String title) {
    title = filterContent(title);
    write((title == null) ? "":title);
  }//link

  public void title(String title) {
    write(filterContent(title));
  }//title

  public void beginSection(String title, int level) {
    write(filterContent(title));
    linebreak();
  }//beginSection

  public void begin_table() {
  }//begin_table

  public void end_table() {
  }//end_table

  public void col() {
    write("");
  }//col

  public void row() {
    linebreak();
  }//row

  protected String filterContent(String q) {
    return MarkupLanguageFilter.filterMLWS(q);
  }//filterContent

  public static final String LINEBREAK = "\n";

}//class TextTarget
