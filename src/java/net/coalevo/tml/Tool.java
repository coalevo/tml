/*
 [Adapted from BSD licence]
 Copyright (c) 2002-2004 Terence Parr
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package net.coalevo.tml;

import antlr.TokenStreamException;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Tool {

  public static String version = "0.1";

  public static void main(String[] args) throws Exception {
    if (args == null || args.length  < 2) {
      printUsage();
      System.exit(-1);
    }

    TMLTranslator target = null;
    String infile = "stdin";
    String outfile = "stdout";

    Map<String, String> vars = new HashMap<String, String>();

    for (int i = 0; i < args.length; i++) {
      String arg = args[i];
      if (arg.startsWith("-d")) {
        String a = arg.substring(2);
        int eq = a.indexOf('=');
        if (eq < 0) {
          System.err.println("missing '=' in assignment for -D");
          printUsage();
          System.exit(-1);
        }
        String id = a.substring(0, eq);
        String value = a.substring(eq + 1, a.length());
        System.out.println("override " + id + "=" + value);
        vars.put(id, value);
        continue;
      }

      //Targets
      if (arg.startsWith("-target=")) {
        String a = arg.substring(8);
        if ("ansi".equals(a)) {
          target = new ANSITarget();
        }
        if ("html".equals(a)) {
          target = new HTMLTarget();
        }
        if("bbcode".equals(a)) {
          target = new BBCodeTarget();
        }
        if("telnetd".equals(a)) {
          target = new TelnetDTarget();
        }
        if("text".equals(a)) {
          target = new TextTarget();
        }
        continue;
      }

      //Input and Output
      if (arg.startsWith("-in=")) {
        infile = arg.substring(4);
      }
      if (arg.startsWith("-out=")) {
        outfile = arg.substring(5);
      }
    }
    //2. default target
    if(target==null) {
      target = new HTMLTarget();
    }
    //3. Input
    Reader r = null;
    if ("stdin".equals(infile)) {
      r = new InputStreamReader(System.in);
    } else {
      r = new FileReader(infile);
    }
    //4. Output
    Writer w = null;
    if ("stdout".equals(outfile)) {
      w = new PrintWriter(System.out);
    } else {
      w = new FileWriter(outfile);
    }
    target.setWriter(w);


    DefaultTMLEngine engine = new DefaultTMLEngine(r);
    engine.getContext().pushInputName(infile);
    engine.setFilename("infile");

    try {
      engine.translate(target);
    } catch (Exception atse) {
      System.err.println("TML lexer error: " + atse);
    }
  }//main

  public static void printMessage() {
    System.err.println("TML Version " + version + "(c) 2006 VFI and (c) 2002-2004 antlr.org");
  }//printMessage

  public static void printUsage() {
    System.err.println("Usage: java net.coalevo.tml.Tool [args]");
    System.err.println("  -in=                            input filename (stdin for console; default).");
    System.err.println("  -out=                           output filename (stdout for console; default).");
    System.err.println("  -target=[html|ansi|bbcode|text] output target (default is html).");
    System.err.println("  -dx=y                           predefine TML variable x to be y.");
  }//printUsage

}//Tool
