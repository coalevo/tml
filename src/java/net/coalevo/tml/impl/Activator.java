/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml.impl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import net.coalevo.text.model.Transformer;
import net.coalevo.tml.TMLTransformer;

/**
 * Provides the bundle activator implementation for this
 * bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext)
      throws Exception {

    if(m_StartThread!=null && m_StartThread.isAlive()) {
      throw new Exception();
    }
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            try {
              TMLTransformer t = new TMLTransformer();
              registerTransformer(bundleContext, new TML2HTMLTransformer(t));
              registerTransformer(bundleContext, new TML2ANSITransformer(t));
              registerTransformer(bundleContext, new TML2TelnetDTransformer(t));
              registerTransformer(bundleContext, new TML2PlainTransformer(t));
            } catch (Exception ex) {
              ex.printStackTrace(System.err);
            }
          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext)
      throws Exception {
   //wait start
    if(m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }
    //unregister should be automatic.
  }//stop

  private void registerTransformer(BundleContext bc, Transformer t) {
    bc.registerService(
        Transformer.class.getName(),
        t,
        null);
  }//registerTransformer

}//class Activator
