/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml.impl;

import net.coalevo.text.model.Transformer;
import net.coalevo.text.model.BaseTransformer;
import net.coalevo.text.model.TransformationException;
import net.coalevo.tml.TMLTransformer;
import net.coalevo.tml.TMLException;

/**
 * Implements a {@link Transformer} that transforms
 * from TML to HTML.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class TML2HTMLTransformer
    extends BaseTransformer {

    private TMLTransformer m_Transformer;

    public TML2HTMLTransformer(TMLTransformer t) {
      super("tml","html");
      m_Transformer = t;
    }//constructor

    public String transform(String input)
        throws TransformationException {
      try {
        return m_Transformer.toHTML(input);
      } catch (TMLException e) {
        throw new TransformationException(e.getMessage(),e.getLine());
      }
    }//transform

}//class TML2HTMLTransformer
