/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Verein zur Foerderung der Internetkommunikation, Austria
 * http://www.vfi.or.at
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.tml;

import net.coalevo.text.util.MarkupLanguageFilter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

class BBCodeTarget
    extends NOPTarget
    implements TMLTranslator {

  private Table m_Table;
  private StringBuilder m_Col;

  public BBCodeTarget() {
    this(new BufferedWriter(new OutputStreamWriter(System.out)));
  }//constructor

  public BBCodeTarget(Writer out) {
    this.m_Out = out;
  }//constructor

  public String getTargetLanguage() {
    return "BBCODE";
  }//getTargetLanguage

  /**
   * insert head.html if found
   */
  public void begin() {
    /*
    writeln("<html>");
    writeln("<head>");
    writeln("<body bgcolor=#FFFFFF text=#000000>");
    */
  }//begin

  /**
   * insert tail.html if found
   */
  public void end() {
    /*
    writeln("</body>");
    writeln("</html>");
    */
    try {
      m_Out.flush();
    }
    catch (IOException ioe) {
      System.err.println("Problem writing HTML output");
      ioe.printStackTrace(System.err);
    }
  }//end

  protected String extractLastCommand(String text) {
    if (text == null) {
      return null;
    }
    int i = text.lastIndexOf("<");
    if (i < 0) {
      return null;
    }
    int begin = i;
    i++;
    int end = 0;
    while (i < text.length() && text.charAt(i) != '>') {
      i++;
    }
    if (i >= text.length()) {
      return null; // <... not terminated
    }
    end = i;
    return text.substring(begin, end);
  }//extractLastCommand

  public void write(String t) {
    if (m_Col != null) {
      t = t.trim();
      if (t.length() > 0) {
        m_Col.append(t);
      }
      return;
    }
    super.write(t);
  }//write

  public void text(String t) {
    if (m_Col != null) {
      write(t);
      return;
    }
    write(MarkupLanguageFilter.encodeMLSpecials(t));
  }//text

  public void bold(String t) {
    writeTemplate(BOLD_TEMPLATE, t);
  }//bold

  public void italic(String t) {
    writeTemplate(ITALIC_TEMPLATE, t);
  }//italic

  public void tt(String t) {
    writeTemplate(TT_TEMPLATE, t);
  }//tt

  public void underlined(String t) {
    writeTemplate(UNDERLINE_TEMPLATE, t);
  }//underline

  public void beginListItem(int level) {
    write(START_LISTITEM);
  }//beginListItem

  public void endListItem(int level) {
    write(END_LISTITEM);
  }//endListItem

  public void begin_ul(int level) {
    write(START_BULLETED_LIST);
  }//begin_ul

  public void end_ul(int level) {
    write(END_BULLETED_LIST);
  }//end_ul

  public void begin_ol(int level) {
    write(START_ALPHANUMERIC_LIST);
  }//begin_ol

  public void end_ol(int level) {
    write(END_ALPHANUMERIC_LIST);
  }//end_ol

  public void paragraph() {
    write(PARAGRAPH);
  }//paragraph

  public void linebreak() {
    write(LINEBREAK);
  }//linebreak

  public void blankline() {
    write(BLANKLINE);
  }//blankline

  public void code(String t) {
    writeTemplate(CODE_TEMPLATE, t);
  }//code

  public void verbatim(String rawOutput) {
    write(rawOutput);
  }//verbatim

  public void blockquote(String t) {
    writeTemplate(BLOCKQUOTE_TEMPLATE, t);
  }//blockquote

  public void link(String url, String title) {
    if (title == null) {
      title = url;
    } else {
      title = MarkupLanguageFilter.encodeMLSpecials(title);
    }
    write(String.format(LINK_TEMPLATE, url, title));
  }//link

  public void beginSection(String title, int level) {
    title = MarkupLanguageFilter.encodeMLSpecials(title);
    title = String.format(HEADER_TEMPLATE, ++level, title);
    write(title);
  }//beginSection


  public void begin_table() {
    m_Table = new BBCodeTable();
    m_Col = new StringBuilder();
  }//begin_table

  public void end_table() {
    m_Table.addCol(m_Col.toString());
    m_Col = new StringBuilder();
    m_Table.nextRow();
    m_Col = null;
    //printout
    super.write(m_Table.toString());
    m_Table = null;
  }//end_table

  public void col() {
    m_Table.addCol(m_Col.toString());
    m_Col = new StringBuilder();
  }//col

  public void row() {
    m_Table.addCol(m_Col.toString());
    m_Col = new StringBuilder();
    m_Table.nextRow();
  }//row

  protected void writeTemplate(String template, String arg) {
    arg = MarkupLanguageFilter.encodeMLSpecials(arg);
    arg = String.format(template, arg);
    write(arg);
  }//writeTemplate

  private static class BBCodeTable
      implements Table {


    protected int m_RIdx;
    protected List<List<String>> m_Rows;

    public BBCodeTable() {
      m_Rows = new ArrayList<List<String>>();
      m_RIdx = 0;
      m_Rows.add(new ArrayList<String>());
    }//constructor

    public void addCol(String str) {
      List<String> cols = m_Rows.get(m_RIdx);
      cols.add(str);
    }//addCol

    public void nextRow() {
      m_Rows.add(new ArrayList<String>());
      m_RIdx++;
    }//nextRow

    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(START_TABLE);
      //3. Add rows
      for (int n = 0; n < m_Rows.size(); n++) {
        List<String> cols = m_Rows.get(n);
        if (cols.isEmpty()) {
          break;
        }
        sb.append(START_TABLE_ROW);
        int numcols = cols.size();
        for (int i = 0; i < numcols; i++) {
          String colstr = cols.get(i);
          sb.append(String.format(TABLE_CELL_TEMPLATE, colstr));
        }
        sb.append(END_TABLE_ROW);
      }

      sb.append(END_TABLE);
      return sb.toString();
    }//toString


  }//class BBCodeTable

  protected static final String BOLD_TEMPLATE = "[b]%s[/b]";
  protected static final String ITALIC_TEMPLATE = "[i]%s[/i]";
  protected static final String TT_TEMPLATE = "[[ %s ]]";
  protected static final String UNDERLINE_TEMPLATE = "[u]%s[/u]";
  protected static final String CODE_TEMPLATE = "[code][[ %s ]]";
  protected static final String PREFORMATTED_TEMPLATE = "[[ %s ]]";
  protected static final String BLOCKQUOTE_TEMPLATE = "[[ %s ]]";
  protected static final String LINK_TEMPLATE = "[url=\"%s\"]%s[/url]";
  protected static final String HEADER_TEMPLATE = "[h%1$d]%2$s[/h%1$d]";

  protected static final String START_LISTITEM = "[li]";
  protected static final String END_LISTITEM = "[/li]";

  protected static final String START_ALPHANUMERIC_LIST = "[list=#]";
  protected static final String END_ALPHANUMERIC_LIST = "[/list]";

  protected static final String START_BULLETED_LIST = "[list=*]";
  protected static final String END_BULLETED_LIST = "[/list]";

  protected static final String LINEBREAK = "[br]";
  protected static final String BLANKLINE = "[br][br]";

  protected static final String PARAGRAPH = BLANKLINE;


  protected static final String START_TABLE = "[table]";
  protected static final String END_TABLE = "[/table]";

  protected static final String START_TABLE_ROW = "[tr]";
  protected static final String END_TABLE_ROW = "[/tr]";

  protected static final String TABLE_CELL_TEMPLATE = "[td]%s[/td]";

}//HTMLTarget
